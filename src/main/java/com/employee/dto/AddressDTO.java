package com.employee.dto;
/**
 * 
 * @author XYZA
 *
 */
public class AddressDTO {

	private Integer id;

	private String city;

	public String toString() {
		return "Address [id=" + id + ", city=" + city + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
