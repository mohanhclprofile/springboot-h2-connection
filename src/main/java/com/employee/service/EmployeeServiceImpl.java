package com.employee.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.dto.EmployeeDTO;
import com.employee.entity.Address;
import com.employee.entity.Employee;
import com.employee.repo.EmployeeReposiotory;
/**
 * 
 * @author XYZA
 *
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeReposiotory repo;

	@Override
	public void saveEmployee(EmployeeDTO employeeDTO) {
		// TODO Auto-generated method stub
		Employee employee = new Employee();
		BeanUtils.copyProperties(employeeDTO, employee);
		List<Address> addressList = new ArrayList<Address>();
		employeeDTO.getAddress().stream().forEach(address -> {
			Address add = new Address();
			BeanUtils.copyProperties(address, add);
			addressList.add(add);
		});
		
		employee.setAddress(addressList);
		repo.save(employee);
	}

}
