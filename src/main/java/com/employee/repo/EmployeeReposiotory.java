package com.employee.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.entity.Employee;
/**
 * 
 * @author XYZA
 *
 */
@Repository
public interface EmployeeReposiotory extends JpaRepository<Employee, Integer>{

}
