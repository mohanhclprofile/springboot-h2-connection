package com.employee.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
/**
 * 
 * @author XYZA
 *
 */
@Entity
public class Employee {
	
	@Id
	private Integer empId;
	/**
	 * 
	 */
	@Column(name = "EMPLOYEE_NAME")
	private String empName;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "EMPID")
	private List<Address> address;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", address=" + address + "]";
	}
	
	

}
