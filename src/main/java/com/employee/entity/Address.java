package com.employee.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
/**
 * 
 */
@Entity
public class Address {
	
	@Id
	private Integer id;
	
	private String city;

	@Override
	public String toString() {
		return "Address [id=" + id + ", city=" + city + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	

}
