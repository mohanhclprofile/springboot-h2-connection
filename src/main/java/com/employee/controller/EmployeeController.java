package com.employee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.employee.dto.EmployeeDTO;
import com.employee.service.EmployeeService;
/**
 * 
 * @author HCL
 *
 */
@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService empService;

	/**
	 * 
	 * @param employeeDTO for saveEmployee
	 * @return success message
	 */
	@PostMapping(value = "/addEmployee",consumes ="application/json")
	public String addEmployee(@RequestBody EmployeeDTO employeeDTO) {
		empService.saveEmployee(employeeDTO);
		return "Success";

	}

}
